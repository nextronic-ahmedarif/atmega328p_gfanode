 #include <RGBmatrixPanel.h>

#define ADDR 'a'
#define CMD_RECALL 'a'
#define CMD_NEXT 'b'

#define OE 9
#define LAT 10
#define CLK 11
#define A 8
#define C 12
#define B 13
const char str[] PROGMEM = "<< Au suivant <<";
const char strHalt[] PROGMEM = " Hors Service ";
const char strnull[] PROGMEM = "---";

RGBmatrixPanel matrix(A, B, C, CLK, LAT, OE, false);
#define F2(progmem_ptr) (const __FlashStringHelper *)progmem_ptr



void serialFlush() {
  while (Serial.available() > 0) {
    Serial.read();
  }
}
int16_t    textX         = matrix.width(),
           hue           = 0;
char * txUart;
int steps = 0;
char * sNumber;
void setup() {
  matrix.begin();
  matrix.setTextWrap(false);
  matrix.setTextSize(2);
  Serial.begin(115200);
  steps = 1;
  *sNumber = malloc(3);
  sNumber = "###";
   *txUart = malloc(5);
  txUart = "aI000";
  txUart[0] = ADDR;
}

static int count = 0;
byte i;
char inChar ;
char cmd;
void loop() {
  //Serial.write(txUart);
  if (Serial.available() && steps == 3) {
    if (Serial.read() == ADDR ) {
      while (!Serial.available());
      cmd = (char)Serial.read();
      if (cmd != 'I') {
        steps = 4;
      }
    }
  }
  if (steps == 1) {
    matrix.fillScreen(0);
    matrix.setTextColor(matrix.ColorHSV(hue, 255, 255, true));
    matrix.setCursor(textX, 1);
    matrix.print(F2(str));
    if ((--textX) < -204) {
      textX = matrix.width();
      steps = 2;
      if (Serial.available()) {
        sNumber[0] = Serial.read();
        sNumber[1] = Serial.read();
        sNumber[2] = Serial.read();
        serialFlush();
      }
    }
    hue += 7;
    if (hue >= 1536) hue -= 1536;
    matrix.swapBuffers(false);
  }
  if (steps == 2) {
    matrix.fillScreen(0);
    matrix.setTextColor(matrix.Color888(255, 255, 255));
    matrix.setCursor(0 + textX, 1);
    matrix.print(sNumber[0]);
    matrix.setCursor(11 + textX, 1);
    matrix.print(sNumber[1]);
    matrix.setCursor(22 + textX, 1);
    matrix.print(sNumber[2]);
    if ((--textX) < 0) {
      steps = 3;
    }
    hue += 7;
    if (hue >= 1536) hue -= 1536;
    matrix.swapBuffers(false);
  }
  if (steps == 4) {
    matrix.fillScreen(0);
    matrix.setTextColor(matrix.Color888(255, 255, 255));
    matrix.setCursor(0 + textX, 1);
    matrix.print(sNumber[0]);
    matrix.setCursor(11 + textX, 1);
    matrix.print(sNumber[1]);
    matrix.setCursor(22 + textX, 1);
    matrix.print(sNumber[2]);
    if ((--textX) < -48) {
      textX = matrix.width();
      if (cmd == 'P') {
        steps = 1;
      }
      if (cmd == 'H') {
        steps = 5;
      }
    }
    hue += 7;
    if (hue >= 1536) hue -= 1536;
    matrix.swapBuffers(false);
  }
  if (steps == 5) {
    matrix.fillScreen(0);
    matrix.setTextColor(matrix.ColorHSV(hue, 255, 255, true));
    matrix.setCursor(textX, 1);
    matrix.print(F2(strHalt));
    if ((--textX) < -228) {
      textX = matrix.width();
      steps = 2;
      sNumber[0] = strnull[0];
      sNumber[1] = strnull[1];
      sNumber[2] = strnull[2];
    }
    hue += 7;
    if (hue >= 1536) hue -= 1536;
    matrix.swapBuffers(false);
  }
}
